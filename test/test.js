const request = require('supertest');
const app = require('../app');

describe('App', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect(/Welcome to Express/, done);
  });
  it.skip('will fail', () => {
    expect(true).to.equal(false);
  });
}); 
